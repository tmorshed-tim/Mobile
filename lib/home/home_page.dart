import 'package:flutter/material.dart';
import 'dart:convert' show ascii, base64, json, jsonDecode;
import 'package:http/http.dart' as http;
import 'package:http/http.dart';
import 'package:mobile/model/reckoning_mode.dart';
import 'package:mobile/reckoning_create/reckoning_create.dart';
import 'package:mobile/reckonings_list/reckonings_list_page.dart';

class HttpService {
  final String postsURL = "http://localhost:8000/reckonings/";

  Future<List<ReckoningModel>> getReckonings() async {
    final String token = await storage.read(key: 'token');
    Response res = await get(postsURL, headers: {
      'Accept': 'application/json',
      'Authorization': 'Bearer $token',
    });

    if (res.statusCode == 200) {
      List<dynamic> body = jsonDecode(res.body);

      List<ReckoningModel> reckonings = body
          .map(
            (dynamic item) => ReckoningModel.fromJson(item),
          )
          .toList();

      return reckonings;
    } else {
      throw "Unable to create reckoning.";
    }
  }
}

class HomePage extends StatelessWidget {
  HomePage(this.jwt, this.payload);

  factory HomePage.fromBase64(String jwt) => HomePage(
      jwt,
      json.decode(
          ascii.decode(base64.decode(base64.normalize(jwt.split(".")[1])))));

  final String jwt;
  final Map<String, dynamic> payload;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text("Home Page"),
      ),
      body: Center(
        child: Column(
          children: <Widget>[
            Container(
              height: 50,
              width: 250,
              decoration: BoxDecoration(
                  color: Colors.blue, borderRadius: BorderRadius.circular(20)),
              child: FlatButton(
                onPressed: () async {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => ReckoningsList()));
                },
                child: Text(
                  "Reckonings list",
                  style: TextStyle(color: Colors.white, fontSize: 25),
                ),
              ),
            ),
            Container(
              height: 50,
              width: 250,
              decoration: BoxDecoration(
                  color: Colors.blue, borderRadius: BorderRadius.circular(20)),
              child: FlatButton(
                onPressed: () async {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => ReckoningCreate()));
                },
                child: Text(
                  "New reckoning",
                  style: TextStyle(color: Colors.white, fontSize: 25),
                ),
              ),
            ),
            SizedBox(
              height: 130,
            ),
          ],
        ),
      ),
    );
  }
}
