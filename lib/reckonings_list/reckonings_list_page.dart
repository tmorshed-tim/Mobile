import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:mobile/model/reckoning_mode.dart';
import 'dart:async';
import 'dart:convert';
import 'dart:developer';
import 'dart:core';
import 'package:flutter/foundation.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:mobile/reckoning_detail/reckoning_detail.dart';

final storage = FlutterSecureStorage();

class HttpService {
  final String postsURL = "http://localhost:8000/reckonings/";

  Future<List<ReckoningModel>> getReckonings() async {
    final String token = await storage.read(key: 'token');
    Response res = await get(postsURL, headers: {
      'Accept': 'application/json',
      'Authorization': 'Bearer $token',
    });

    if (res.statusCode == 200) {
      List<dynamic> body = jsonDecode(res.body);

      List<ReckoningModel> reckonings = body
          .map(
            (dynamic item) => ReckoningModel.fromJson(item),
          )
          .toList();

      return reckonings;
    } else {
      throw "Unable to retrieve posts.";
    }
  }
}

class ReckoningsList extends StatelessWidget {
  final HttpService httpService = HttpService();

  void initState() {}

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Reckonings list"),
      ),
      body: FutureBuilder(
        future: httpService.getReckonings(),
        builder: (BuildContext context,
            AsyncSnapshot<List<ReckoningModel>> snapshot) {
          if (snapshot.hasData) {
            List<ReckoningModel>? reckonings = snapshot.data;
            return ListView(
              children: reckonings!
                  .map(
                    (ReckoningModel reckoning) => ListTile(
                      title: Text(reckoning.number),
                      subtitle: Text(
                          "Issue date:${reckoning.dateOfIssue}; Status: ${reckoning.status}; Comment: ${reckoning.comment}"),
                      onTap: () => Navigator.of(context).push(
                        MaterialPageRoute(
                          builder: (context) =>
                              ReckoningDetail(reckoning: reckoning),
                        ),
                      ),
                    ),
                  )
                  .toList(),
            );
          } else {
            return Center(child: CircularProgressIndicator());
          }
        },
      ),
    );
  }
}
