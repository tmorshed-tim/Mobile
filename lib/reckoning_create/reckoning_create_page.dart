import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:mobile/model/reckoning_mode.dart';
import 'package:mobile/reckonings_list/reckonings_list_page.dart';

class HttpService {
  final String postsURL = "http://localhost:8000/reckonings/";

  Future<String> createReckoning(ReckoningModel reckoning) async {
    print(reckoning.toJson());
    print(json.encode(reckoning));
    final String token = await storage.read(key: 'token');
    Response res = await post(postsURL,
        headers: {
          'Accept': 'application/json',
          'Authorization': 'Bearer $token',
        },
        body: json.encode(reckoning));
    print(res.statusCode);
    print(res.body);
    if (res.statusCode == 201) {
      return "created";
    } else {
      throw "Unable to create reckoning.";
    }
  }
}

class ReckoningCreate extends StatelessWidget {
  final HttpService httpService = HttpService();

  final _number = TextEditingController();

  final _buyer_name = TextEditingController();
  final _buyer_address = TextEditingController();
  final _buyer_uuid = TextEditingController();

  final _seller_name = TextEditingController();
  final _seller_address = TextEditingController();

  final _place_of_sale = TextEditingController();
  final _date_of_issue = TextEditingController();
  final _date_of_payment = TextEditingController();
  final _date_sale = TextEditingController();

  final _status = TextEditingController();
  final _comment = TextEditingController();

  final _person_issuer = TextEditingController();
  final _person_buyer = TextEditingController();

  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    // Build a Form widget using the _formKey created above.
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          title: Text("New Reckoning"),
        ),
        body: Form(
          key: _formKey,
          child: ListView(
            children: <Widget>[
              Text('Number'),
              TextFormField(
                // The validator receives the text that the user has entered.
                controller: _number,
                validator: (_number) {
                  if (_number == null || _number.isEmpty) {
                    return 'Number cannot be empty';
                  }
                  return null;
                },
              ),
              Text('Buyer name'),
              TextFormField(
                // The validator receives the text that the user has entered.
                controller: _buyer_name,
                validator: (_buyer_name) {
                  if (_buyer_name == null || _buyer_name.isEmpty) {
                    return 'Buyer name cannot be empty';
                  }
                  return null;
                },
              ),
              Text('Buyer address'),
              TextFormField(
                // The validator receives the text that the user has entered.
                controller: _buyer_address,
                validator: (_buyer_address) {
                  if (_buyer_address == null || _buyer_address.isEmpty) {
                    return 'Buyer address cannot be empty';
                  }
                  return null;
                },
              ),
              Text('Buyer uuid'),
              TextFormField(
                // The validator receives the text that the user has entered.
                controller: _buyer_uuid,
                validator: (_buyer_uuid) {
                  if (_buyer_uuid == null || _buyer_uuid.isEmpty) {
                    return 'Buyer uuid cannot be empty';
                  }
                  if (_buyer_uuid.length != 36) {
                    return 'Buyer uuid must be exacly 36';
                  }
                  return null;
                },
              ),
              Text('Seller name'),
              TextFormField(
                // The validator receives the text that the user has entered.
                controller: _seller_name,
                validator: (_seller_name) {
                  if (_seller_name == null || _seller_name.isEmpty) {
                    return 'Seller name cannot be empty';
                  }
                  return null;
                },
              ),
              Text('Seller address'),
              TextFormField(
                // The validator receives the text that the user has entered.
                controller: _seller_address,
                validator: (_seller_address) {
                  if (_seller_address == null || _seller_address.isEmpty) {
                    return 'Seller address cannot be empty';
                  }
                  return null;
                },
              ),
              Text('Place of sale'),
              TextFormField(
                // The validator receives the text that the user has entered.
                controller: _place_of_sale,
                validator: (_place_of_sale) {
                  if (_place_of_sale == null || _place_of_sale.isEmpty) {
                    return 'Place of sale cannot be empty';
                  }
                  return null;
                },
              ),
              Text('Date of issue'),
              TextFormField(
                // The validator receives the text that the user has entered.
                controller: _date_of_issue,
                validator: (_date_of_issue) {
                  if (_date_of_issue == null || _date_of_issue.isEmpty) {
                    return 'Date of issue cannot be empty';
                  }
                  if (_date_of_issue.length < 7) {
                    return 'Date of issue must be longer than 7';
                  }
                  return null;
                },
              ),
              Text('Date of payment'),
              TextFormField(
                // The validator receives the text that the user has entered.
                controller: _date_of_payment,
                validator: (_date_of_payment) {
                  if (_date_of_payment == null || _date_of_payment.isEmpty) {
                    return 'Date of payment cannot be empty';
                  }
                  if (_date_of_payment.length < 7) {
                    return 'Date of payment must be longer than 7';
                  }
                  return null;
                },
              ),
              Text('Date sale'),
              TextFormField(
                // The validator receives the text that the user has entered.
                controller: _date_sale,
                validator: (_date_sale) {
                  if (_date_sale == null || _date_sale.isEmpty) {
                    return 'Date sale cannot be empty';
                  }
                  if (_date_sale.length < 7) {
                    return 'Date sale must be longer than 7';
                  }
                  return null;
                },
              ),
              Text('Status'),
              TextFormField(
                // The validator receives the text that the user has entered.
                controller: _status,
                validator: (_status) {
                  if (_status == null || _status.isEmpty) {
                    return 'Status cannot be empty';
                  }
                  return null;
                },
              ),
              Text('Comment'),
              TextFormField(
                // The validator receives the text that the user has entered.
                controller: _comment,
                validator: (_comment) {
                  if (_comment == null || _comment.isEmpty) {
                    return 'Comment cannot be empty';
                  }
                  return null;
                },
              ),
              Text('Person issuer'),
              TextFormField(
                // The validator receives the text that the user has entered.
                controller: _person_issuer,
                validator: (_person_issuer) {
                  if (_person_issuer == null || _person_issuer.isEmpty) {
                    return 'Person issuer cannot be empty';
                  }
                  return null;
                },
              ),
              Text('Person buyer'),
              TextFormField(
                // The validator receives the text that the user has entered.
                controller: _person_buyer,
                validator: (_person_buyer) {
                  if (_person_buyer == null || _person_buyer.isEmpty) {
                    return 'Person buyer cannot be empty';
                  }
                  return null;
                },
              ),
              ElevatedButton(
                onPressed: () {
                  if (_formKey.currentState!.validate()) {
                    Buyer buyer = new Buyer(
                        name: _buyer_name.text,
                        address: _buyer_address.text,
                        uuid: _buyer_uuid.text);
                    Seller seller = new Seller(
                        name: _seller_name.text, address: _seller_address.text);
                    ReckoningModel reckoning = new ReckoningModel(
                        number: _number.text,
                        buyer: buyer,
                        seller: seller,
                        placeOfSale: _place_of_sale.text,
                        dateOfIssue: _date_of_issue.text,
                        dateOfPayment: _date_of_payment.text,
                        dateSale: _date_sale.text,
                        status: _status.text,
                        comment: _comment.text,
                        personIssuer: _person_issuer.text,
                        personBuyer: _person_buyer.text);

                    httpService.createReckoning(reckoning);
                  }
                },
                child: Text('Submit'),
              ),
            ],
          ),
        ));
  }
}
