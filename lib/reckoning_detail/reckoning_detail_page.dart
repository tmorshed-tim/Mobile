import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:mobile/model/reckoning_mode.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:mobile/reckonings_list/reckonings_list.dart';

class HttpService {
  // final String URL = "http://localhost:8000/reckonings/";

  Future<String> deleteReckoning(String number) async {
    final String token = await storage.read(key: 'token');
    //final parameters = {"reckoning_number": number};

    /*final url = Uri.parse(URL);
    final request = http.Request("DELETE", url);
    request.headers.addAll(<String, String>{
      'Accept': 'application/json',
      'Authorization': 'Bearer $token',
    });*/
    //request.body = jsonEncode({'reckoning_number': number});

    http.Response res =
        await http.delete('http://localhost:8000/reckonings/$number', headers: {
      'Accept': 'application/json',
      'Authorization': 'Bearer $token',
    });

    //final res = await request.send();
    //print(request.body);
    //print(res.statusCode);
    //print(res.toString());
    if (res.statusCode == 204) {
      return res.body;
    } else {
      throw "Unable to delete reckoning.";
    }
  }
}

class ReckoningDetail extends StatelessWidget {
  final ReckoningModel reckoning;

  final HttpService httpService = HttpService();

  ReckoningDetail({required this.reckoning});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Reckoning detail"),
        ),
        body: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(12.0),
            child: Column(
              children: <Widget>[
                Card(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      ListTile(
                        title: Text("Reckoning number"),
                        subtitle: Text("${reckoning.number}"),
                      ),
                      ListTile(
                        title: Text("Buyer name"),
                        subtitle: Text("${reckoning.buyer.name}"),
                      ),
                      ListTile(
                        title: Text("Buyer address"),
                        subtitle: Text("${reckoning.buyer.address}"),
                      ),
                      ListTile(
                        title: Text("Seller name"),
                        subtitle: Text("${reckoning.seller.name}"),
                      ),
                      ListTile(
                        title: Text("Seller address"),
                        subtitle: Text("${reckoning.seller.address}"),
                      ),
                      ListTile(
                        title: Text("Date isuue"),
                        subtitle: Text("${reckoning.dateOfIssue}"),
                      ),
                      ListTile(
                        title: Text("Date of payment"),
                        subtitle: Text("${reckoning.dateOfPayment}"),
                      ),
                      ListTile(
                        title: Text("Date sale"),
                        subtitle: Text("${reckoning.dateSale}"),
                      ),
                      ListTile(
                        title: Text("Status"),
                        subtitle: Text("${reckoning.status}"),
                      ),
                      ListTile(
                        title: Text("Comment"),
                        subtitle: Text("${reckoning.comment}"),
                      ),
                      ListTile(
                        title: Text("Person issuer"),
                        subtitle: Text("${reckoning.personIssuer}"),
                      ),
                      ListTile(
                        title: Text("Person buyer"),
                        subtitle: Text("${reckoning.personBuyer}"),
                      ),
                      ElevatedButton(
                        onPressed: () {
                          httpService.deleteReckoning(reckoning.number);
                        },
                        child: Text('Delete'),
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
        ));
  }
}
