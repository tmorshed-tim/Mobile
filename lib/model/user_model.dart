class User {
  String username;
  String token;
  String role;

  User({this.username, this.token, this.role});

  factory User.fromDatabaseJson(Map<String, dynamic> data) =>
      User(username: data['name'], token: data['token'], role: data['role']);
}
