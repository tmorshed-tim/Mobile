// To parse this JSON data, do
//
//     final welcome = welcomeFromJson(jsonString);

import 'dart:convert';

List<ReckoningModel> welcomeFromJson(String str) => List<ReckoningModel>.from(
    json.decode(str).map((x) => ReckoningModel.fromJson(x)));

String welcomeToJson(List<ReckoningModel> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class ReckoningModel {
  ReckoningModel({
    required this.number,
    required this.buyer,
    required this.seller,
    required this.placeOfSale,
    required this.dateOfIssue,
    required this.dateOfPayment,
    required this.dateSale,
    required this.status,
    required this.comment,
    required this.personIssuer,
    required this.personBuyer,
  });

  String number;
  Buyer buyer;
  Seller seller;
  String placeOfSale;
  String dateOfIssue;
  String dateOfPayment;
  String dateSale;
  String status;
  String comment;
  String personIssuer;
  String personBuyer;

  factory ReckoningModel.fromJson(Map<String, dynamic> json) => ReckoningModel(
        number: json["number"],
        buyer: Buyer.fromJson(json["buyer"]),
        seller: Seller.fromJson(json["seller"]),
        placeOfSale: json["place_of_sale"],
        dateOfIssue: json["date_of_issue"],
        dateOfPayment: json["date_of_payment"],
        dateSale: json["date_sale"],
        status: json["status"],
        comment: json["comment"],
        personIssuer: json["person_issuer"],
        personBuyer: json["person_buyer"],
      );

  Map<String, dynamic> toJson() => {
        "number": number,
        "buyer": buyer.toJson(),
        "seller": seller.toJson(),
        "place_of_sale": placeOfSale,
        "date_of_issue": dateOfIssue,
        "date_of_payment": dateOfIssue,
        "date_sale": dateSale,
        "status": status,
        "comment": comment,
        "person_issuer": personIssuer,
        "person_buyer": personBuyer,
      };
}

class Buyer {
  Buyer({
    required this.name,
    required this.address,
    required this.uuid,
  });

  String name;
  String address;
  String uuid;

  factory Buyer.fromJson(Map<String, dynamic> json) => Buyer(
        name: json["name"],
        address: json["address"],
        uuid: json["uuid"],
      );

  Map<String, dynamic> toJson() => {
        "name": name,
        "address": address,
        "uuid": uuid,
      };
}

class Seller {
  Seller({
    required this.name,
    required this.address,
  });

  String name;
  String address;

  factory Seller.fromJson(Map<String, dynamic> json) => Seller(
        name: json["name"],
        address: json["address"],
      );

  Map<String, dynamic> toJson() => {
        "name": name,
        "address": address,
      };
}
